ifBPGcm.spi
===========

ifBPG.spiを埋め込みICCプロファイルに対応させたものです。プロファイルの引渡しに対応したホストアプリケーションにおいてカラーマネージメント表示が可能です。

動作確認済みホストは以下の通りです。他のホストでも基本的に動作するはずですが，カラーマネジメント表示を行うにはホスト側の対応が必要です。

* [susico](http://blog.livedoor.jp/yamma_ma/archives/43141231.html)
* [PictureFan](http://www.geocities.jp/iooiau/picturefan.html)

オリジナルのifBPG.spiについては https://github.com/256bai/ifbpg を参照してください。

（以下原文）

BPG用のSusie plug-in

ひな形のソースを使わせて頂きました。

http://www.asahi-net.or.jp/~kh4s-smz/spi/make_spi.html

build bat
---------
* [libbpg-0.9.5.tar.gz](http://bellard.org/bpg/)
* [i686-4.9.2-release-posix-dwarf-rt_v3-rev0.7z](http://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/4.9.2/threads-posix/dwarf/i686-4.9.2-release-posix-dwarf-rt_v3-rev0.7z/download)


* \libbpg-0.9.5\ 
* \mingw32\ 
* \ifbpg\build_ifBPGspi.bat
* \ifbpg\ifBPG.spi