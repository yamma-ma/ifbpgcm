CC=gcc
CFLAGS=-O2 -W -DCONFIG_BPG_VERSION=\"$(CONFIG_BPG_VERSION)\"

all: ifBPGcm
ifBPGcm: spi00in.o spiBPG_ex.o
	$(CC) $(LDFLAGS) -o ifBPGcm.spi -shared -s $^ -lbpg -Wl,--kill-at -Wl,-s

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	$(RM) *.o
	